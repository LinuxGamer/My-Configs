# My Dotfiles
This is a collection of all my dotfiles. They are not for everyone, and I'm probably the only one who will find these useful, but they are here in the public domain now so anyone can use them.

## Current Setup
|  |  |
| ---- | ---- |
| Distro | Fedora 38 Workstation Edition |
| DE/WM | Hyprland |
| Web Browser | Firefox |
| File Manager (GUI) | Thunar |
| File Manager (TUI) | Ranger |
| Terminal | Foot |
| Application Launcher | Wofi |
| Bar | Waybar |
| Terminal Text Editor | Neovim |
| GUI Text Editor | Kate |
| Notifications Daemon | Mako |
| Office Suite | Calligra |
