# sway config

set $mod Mod4

font pango:fontawesome 8

exec_always --no-startup-id ~/.local/bin/autotiling
exec_always --no-startup-id swaybg -i ~/.Pictures/Wallpapers/IMG_1923.jpg -m fit
exec_always --no-startup-id mako
exec_always wl-clipboard-history -t
exec_always --no-startup-id source ~/.bash_profile
exec_always --no-startup-id /usr/libexec/polkit-gnome-authentication-agent-1
exec_always --no-startup-id dbus-update-activaiton-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

exec --no-startup-id xss-lock --transfer-sleep-lock -- swaylock --nofork
exec --no-startup-id nm-applet

input 2:7:SynPS/2_Synaptics_TouchPad {
	tap enabled
	natural_scroll disabled
}
input 1:1:AT_Translated_Set_2_keyboard {
    xkb_layout "gb"
}
input 3690:5664:Gaming_KB_Gaming_KB {
    xkb_layout "gb"
}

exec swayidle -w \
         timeout 500 'swaylock -f -c 000000' \
         timeout 800 'swaymsg "output * power off"' resume 'swaymsg "output * power on"' \
         before-sleep 'swaylock -f -c 000000'

for_window [class="^.*"] border pixel 3
default_border pixel 3
default_floating_border normal 0
hide_edge_borders smart
gaps inner 4
gaps outer 4
smart_gaps on

set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

floating_modifier $mod

bindsym $mod+Return exec foot
bindsym $mod+t exec foot

bindsym $mod+Tab exec ~/.local/bin/switcher

bindsym $mod+b exec firefox

bindsym $mod+Shift+h exec --no-startup-id python3 ~/.local/bin/keybinds.py
for_window [title="Keybinds List"] floating enable

bindsym $mod+l exec swaylock

bindsym $mod+w kill

bindsym $mod+d exec --no-startup-id wofi --show drun -I --style ~/.config/wofi/style.css

bindsym $mod+e exec --no-startup-id thunar

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+h split h
bindsym $mod+v split v

bindsym $mod+f fullscreen toggle
bindsym $mod+s layout stacking
bindsym $mod+p layout tabbed
bindsym $mod+o layout toggle split
bindsym $mod+space floating toggle

#bindsym $mod+space focus mode_toggle
bindsym $mod+a focus parent

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

bindsym $mod+Shift+c reload

bindsym $mod+Shift+r restart

bindsym $mod+Shift+e exec "swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit Sway? This will end your X session.' -B 'Yes, exit Sway' 'swaymsg exit'"

bindsym $mod+escape exec "shutdown"

mode "resize" {
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt
	
	bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

	bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"
