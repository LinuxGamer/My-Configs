#!/usr/bin/env bash

echo "Updating via DNF!"
sudo dnf upgrade
echo "DNF Update Done!"
echo "Updating via Flatpak!"
flatpak update
echo "Flatpak Update Done!"
read -p "All Done! Press any key to exit"