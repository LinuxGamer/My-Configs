#!/bin/bash

# Define keybinds and commands
KEYBINDS=("Mod1+Shift+t" "Mod1+Shift+e" "Mod1+Shift+q")
COMMANDS=("urxvt" "nautilus" "i3-msg exit")

# Create list of keybinds and commands
LIST=""
for i in "${!KEYBINDS[@]}"; do
  LIST+="${KEYBINDS[i]}: ${COMMANDS[i]}\n"
done

# Run alacritty in floating window using Sway's "floating enable" command
swaymsg "floating enable, resize set 800px 400px, border pixel 2px; exec urxvt -e sh -c 'echo -e \"$LIST\" | less'"

# Note: you may need to adjust the window size and border thickness to fit your preferences
